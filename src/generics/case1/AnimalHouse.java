package generics.case1;

/**
 * Created by panda on 2016-11-28.
 */
public class AnimalHouse<E extends Animal> {
    private E animal;

    private E getAnimal() {
        return this.animal;
    }

    private void setAnimal(E e) {
        this.animal = e;
    }


    public static void main(String[] args) {
        //BE CAREFUL:
        //AnimalHouse<Animal> house = new AnimalHouse<Cat>();
        //AnimalHouse<Cat> house = new AnimalHouse<MiniCat>();
        //Generic type must be the exactly same type

        AnimalHouse<?> house = new AnimalHouse<Cat>();

    }
}
