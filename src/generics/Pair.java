package generics;

/**
 * Created by panda2ici on 27/11/16.
 */
public class Pair<A,B> {
    A first;
    B second;

    public Pair(A a, B b){
        first = a;
        second = b;
    }

    public static void main(String[] args) {
        Pair<String, Double> bid;
        bid = new Pair<>("ORCL", 32.07);
        bid = new Pair<String, Double>("ORCL", 32.07);
        bid = new Pair("ORCL",32.07);

        //https://www.ibm.com/developerworks/java/library/j-jtp01255/index.html
        Pair<String, Double>[] holdings;
        //holdings = new Pair<String, Double>[25];
        holdings = new Pair[25];
        holdings[0] = new Pair<String, Double>("ORCL", 32.07);
        holdings[0] = new Pair("ORCL", 32.07);
    }
}
