package package_access.subpackage;

import package_access.BaseClass;

public class SubClass {

	public void test(){
		BaseClass bc = new BaseClass();
		//bc.x;
		//cannot access, because they are not in the *SAME* package.
	}
}
