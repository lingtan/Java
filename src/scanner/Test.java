package scanner;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by panda2ici on 29/11/16.
 */
public class Test {
    public static void main(String[] args) {
        System.out.print("Please input a new serial number:");
        Scanner scanner = new Scanner(System.in);
        System.out.println(scanner.nextLong());
        //scanner.close();
        // Closes the System.in, not just the scanInt object.
        // This means that after the first call to add, the scan object will only consume the input it already has and then you'll receive a NoSuchElementException
        System.out.print("Please input a new serial number:");
        System.out.println(scanner.nextLong());
        scanner.close();
    }
}
