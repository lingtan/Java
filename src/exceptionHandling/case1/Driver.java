package exceptionHandling.case1;

public class Driver {

	public static void main(String[] args) {

	}

	// We can throw Exception because of nothing.
	public static String testException1() throws Exception {
		return "";
	}

	public static String testException2() throws Exception {
		throw new Exception();
	}

	public static String testException3() throws Exception {
		try {
			throw new Exception();
		} catch (Exception e) {

		}
		return "";
	}
}
