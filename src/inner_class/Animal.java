package inner_class;

public class Animal {

	public class InnerCat {
	};

	public static void main(String args) {
		Animal animal = new Animal();
		Animal.InnerCat cat = animal.new InnerCat();
		
		InnerCat cat1 = animal.new InnerCat();

		// Animal.InnerCat cat = new animal.InnerCat();
	}
}
