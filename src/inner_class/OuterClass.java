package inner_class;

/**
 * Created on 2016-12-19.
 *
 * @author Ling Tan
 */
public class OuterClass {
    private static int x;
    private int y;

    private class InnerClass{
        private int y;
        private void test(){
            int x = OuterClass.x;
            int x1 = OuterClass.this.x;
//            int y = OuterClass.y;
            int y1 = OuterClass.this.y;
            int y2 = this.y;
        }
    }
}
