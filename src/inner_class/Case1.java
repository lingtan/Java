package inner_class;

public class Case1 {

	int a1 = Case1.inner.d;
	private static class inner {
		public int a;
		protected int b;
		private int c;
		static int d;
	}
}
