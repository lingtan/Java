package inheritance.case1;

public class BaseClass {

	public static int static_variable = 1;
	protected int x;
	private int y;

	public void test() {
		int x0 = this.x;
		int y0 = this.y;
		BaseClass bc = new BaseClass();
		int x1 = bc.x;
		int y1 = bc.y;
	}
}
