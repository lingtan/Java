package inheritance.case1;

/**
 * Created by Ling Tan on 2017-02-27.
 */
public class Driver {
    public static void main(String[] args) {
        BaseClass a = new BaseClass();
        DerivedClass b = new DerivedClass();
        BaseClass c = new DerivedClass();
        System.out.println(a.static_variable);
        System.out.println(b.static_variable);
        System.out.println(c.static_variable);
    }
}
