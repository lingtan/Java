package inheritance.case1;

public class DerivedClass extends BaseClass {
    public static int static_variable = 2;

    public void test() {
        BaseClass bc = new BaseClass();
        int y = this.x;
        int x = bc.x;
    }
}
