package inheritance.case1.subpackage;

import inheritance.case1.BaseClass;

public class DerivedSubPackageClass extends BaseClass {

	public void test() {
		BaseClass bc = new BaseClass();
		int x0 = this.x;
		// int x = bc.x;
		// x is defined as protected
		DerivedSubPackageClass dsp = new DerivedSubPackageClass();
		int x1 = dsp.x;
	}
}
