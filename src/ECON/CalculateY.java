package ECON;

public class CalculateY {
    static int getFirst(int a, int b) {
        return a;
    }

    /**
     * Y=C+I+G+NX
     * Y=C+I+G+(Export-Import)
     */

    /**
     * @param args
     */

    private static double _C = 0;
    private static double cF1 = 150;
    private static double tax = 0.8;
    private static double Y = 0;
    private static double T = 0;

    private static double _I = 0;
    private static double interstF1 = 0;
    private static double interestF2 = 0;
    private static double i = 0;

    private static double _G = 0;
    private static double _T = 0;

    private static double _NX = 0;
    private static double _export = 0;
    private static double _import = 0;

    private static double _M = 0;
    private static double mF1 = 0;
    private static double mF2 = 0;

    public static void main(String[] args) {
        cF1 = 105;
        tax = 0.5;
        interstF1 = 1100;
        interestF2 = 5900;
        _G = 510;

        _T = 510;

        _export = 70;
        _import = 50;

        i = 0.015;

        mF1 = 1500;
        mF2 = 19200;

        calculateY();
        calculateM();
        calculateC();
        calculateI();


        Y = 2793;
        calculatei();
    }

    private static void calculateY() {
        _I = interstF1 - interestF2 * i;
        _NX = _export - _import;

        //_C = cF1 + tax*(Y-_T);
        //Y = _C + _I + _G + _NX;

        //Y = cF1 + tax * (Y - _T) + _I + _G + _NX;
        Y = (cF1 + tax * (-_T) + _I + _G + _NX) / (1 - tax);
        System.out.println("Y = " + Y);

    }

    private static void calculateC(){
        _C = cF1 + tax * (Y-_T);
        System.out.println("C = " + _C);
    }

    private static void calculateI() {
        _I = interstF1 - interestF2 * i;
        System.out.println("I = " + _I);
    }

    private static void calculateM() {
        _M = mF1 - mF2 * i;
        System.out.println("M = " + _M);
    }

    private static void calculatei() {
        _NX = _export - _import;
        //interestF2 * i = cF1 + tax * (Y - _T) + interstF1 + _G + _NX - Y;
        i = (cF1 + tax * (Y - _T) + interstF1 + _G + _NX - Y) / interestF2;
        System.out.println("i = " + i);
    }
}
