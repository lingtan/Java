package priority;

public class Test {

	public static void main(String[] args) {
		int x = 1;
		System.out.println("x: " + x);
		int y = x++;
		System.out.println("x: " + x);
		System.out.println("y: " + y);
		char[] cs = {'a', 'b', 'c', 'd', 'e', 'f'};
		System.out.println(cs);
		int index = 2;
		System.out.println(cs[index]);
		cs[index--] = 'z';
		System.out.println(index);
		System.out.println(cs);
	}

}
