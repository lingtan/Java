/**
 * A console application that:
 *  1. Receives a positive integer value from the command line as an argument
 *  2. Calculates the SUM of integer values that are smaller than the input argument, that only contain the digits 0 and 1
 *  3. Output the SUM to the console
 *
 * @author  Ling Tan
 * @date    2018-02-17
 */

public class Calculator {

    // The number that has been given from the command line
    private static int input = 0;

    // The sum of values that are smaller than "input" and contain digits 0 and 1 only
    private static int sum = 0;

    public static void main(String[] args) {

        // Whether the console has received a positive number
        boolean isValidInput = false;

        if (args.length == 0) {
            isValidInput = false;
        } else {
            try {
                input = Integer.parseInt(args[0]);
                if (input < 0) {
                    isValidInput = false;
                } else {
                    isValidInput = true;
                }
            } catch (NumberFormatException e) {
                isValidInput = false;
            }
        }

        if (!isValidInput) {
            System.out.println("Invalid input. The program would be terminated.");
        } else {
            //System.out.println("Input: " + input);

            AnalyticalSummation(input);
            System.out.println("Sum: " + sum);

            /*sum = 0;
            PlainSummation(input);
            System.out.println("Sum: " + sum);*/

        }
    }

    /**
     * Sum the numbers that are smaller than input and only contain 0 and 1 by generating all the possible combination of 0 and 1.
     *
     * @param input
     */
    private static void AnalyticalSummation(final int input) {
        int length = String.valueOf(input).length();
        SumNext(new StringBuilder("0"), length);
        SumNext(new StringBuilder("1"), length);
    }

    /**
     * Sum the integer value of the current string if it starts with "1";
     * then add a leading "0" and "1" to this string respectively and pass it to the next round.
     * e.g.:
     *      First,
     *          1*** would be summed, 0*** would not (because 0*** is equivalent to ***)
     *      Then
     *          1*** wold be added a leading "1" (11***)
     *          1*** wold also be added a leading "0" (01***)
     *          These two "numbers" would be passed to next round to see whether they are still valid to be summed.
     *
     * @param sb the string value of an integer to be summed
     * @param maxLength the max length of the string can be
     */
    private static void SumNext(StringBuilder sb, int maxLength) {
        if (sb.toString().startsWith("1")) {
            int r = Integer.parseInt(sb.toString());
            if (r < input) {
                sum += r;
                if (sum < 0) {// sum is > Integer.MAX_VALUE
                    System.out.println("Input is too large, the sum is out of the range of Integer. The program would be terminated.");
                    System.exit(0);
                }
            }
        }

        if (sb.length() < maxLength) {
            SumNext(new StringBuilder("0").append(sb), maxLength);
            SumNext(new StringBuilder("1").append(sb), maxLength);
        }
    }

    /**
     * Sum the numbers that are smaller than input and only contain 0 and 1 by iterating all the possibilities.
     *
     * @param input
     */
    private static void PlainSummation(final int input) {
        for (int i = input - 1; i >= 0; i--) {
            if (String.valueOf(i).matches("^[01]+$")) {
                sum += i;
                if (sum < 0) {// sum is > Integer.MAX_VALUE
                    System.out.println("Input is too large, the sum is out of the range of Integer. The program would be terminated.");
                    System.exit(0);
                }
            }
        }
    }
}
